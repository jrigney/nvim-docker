From ubuntu:xenial

COPY nvim /root/.config/nvim

RUN apt-get update && \
    apt-get install -y software-properties-common python-dev python-pip python3-dev python3-pip curl git && \
    add-apt-repository ppa:neovim-ppa/stable && \
    apt-get update && \
    apt-get install -y neovim && \
    pip3 install neovim

RUN curl -fLo /root/.config/nvim/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim

RUN nvim +PlugInstall +qa

ENV TERM xterm256-color

WORKDIR /data
ENTRYPOINT ["/usr/bin/nvim"]
