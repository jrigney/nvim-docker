# nvim in a container


## Usage

```shell
alias nv='docker run --rm -it -v $PWD:/data:Z jrigney/nvm:1.0'
nv <filename>
```

## Build
```shell
docker build -t jrigney/nvm:1.0 .
```

## TODO
Change base image to alpine.  It looks like if I switch to Alpine as the base image the image size would go from ~680MB to ~340MB
